App = Ember.Application.create();

// takes rendering context and displays and updates
App.ApplicationView = Ember.View.Extend({
  templateName: 'application'
});

// the rendering context
App.ApplicationController = Ember.Controller.extend();

// You must have root, its the container
App.Router = Ember.Router.extend({
  root: Ember.Route.extend({
    index: Ember.Route.extend({
      route: '/'
    })
  })
});

App.initialize();